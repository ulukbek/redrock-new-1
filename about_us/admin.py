from django.contrib import admin

from about_us.models import *

# Register your models here.

admin.site.register(AboutUs),
admin.site.register(OurTeam),
admin.site.register(Testimonials),
