# coding=utf-8
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from ckeditor.fields import RichTextField


class AboutUs(models.Model):
    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    text = RichTextField(null=True)

    def __unicode__(self):
        return self.text


class OurTeam(models.Model):
    class Meta:
        verbose_name = 'Наша команда'
        verbose_name_plural = 'Наша команда'

    image = models.ImageField(upload_to='image/team', verbose_name='Фотография сотрудника')
    name = models.CharField(max_length=255, verbose_name='Имя сотрудника')
    position = models.CharField(max_length=255, verbose_name='Должность в комании')

    def __unicode__(self):
        return self.name


class Testimonials(models.Model):
    class Meta:
        verbose_name = 'Отзыв клиента'
        verbose_name_plural = 'Отзывы клиентов'

    text = RichTextField(null=True)
    name = models.CharField(max_length=255, verbose_name='Имя')
    image = models.ImageField(upload_to='image/clients', verbose_name='Фотография клиента')

    def __unicode__(self):
        return self.name
