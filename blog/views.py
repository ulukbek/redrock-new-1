# coding=utf-8
from __future__ import unicode_literals

import threading

from django.http import request
from django.shortcuts import render
from django.template import loader

from main.views import generate_view_params
from blog.models import *

from django.db.models.signals import post_save
from django.utils.safestring import mark_safe

from tours.views import send_notification_email


# Create your views here.

def index(request, blog_id):
    blog_id = NewsAndBlog.objects.get(id=blog_id)
    context = {
        'id': blog_id,
        'blog': NewsAndBlog.objects.all(),

    }
    context.update(generate_view_params(request))
    return render(request, 'blog.html', context)


def news_letter(sender, instance, created, **kwargs):
    for item in Subscribers.objects.all():
        t = loader.get_template('partial/_newsletter.html')
        letter = t.render(request)

        thread = threading.Thread(target=send_notification_email,
                                  args=('Новое сообщение', letter, 'wowtsty@gmail.com'))
        thread.start()


post_save.connect(news_letter, sender=NewsAndBlog)
