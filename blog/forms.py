from django import forms

from blog.models import Subscribers


class SubscribeForms(forms.ModelForm):
    class Meta:
        model = Subscribers
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(SubscribeForms, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'class': 'email', 'placeholder': 'Email'})
