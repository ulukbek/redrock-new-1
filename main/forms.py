# coding=utf-8
from django import forms
from things_to_do.models import *


class TripPlannerForms(forms.Form):
    CHOICES = []
    THINGS_CHOICES = [ThingsToDo.objects.all()]
    from_date = forms.CharField(widget=forms.TextInput(attrs={'id': 'from_day'}))
    to_date = forms.CharField(widget=forms.TextInput(attrs={'id': 'until_day'}))
    people_counter = forms.IntegerField(
        widget=forms.NumberInput(attrs={'min': '1', 'value': '1'}))
    children_counter = forms.IntegerField(
        widget=forms.NumberInput(attrs={'min': '1', 'value': '1', 'disabled': 'disabled'}))
    category = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())
    things = forms.ChoiceField(choices=THINGS_CHOICES, widget=forms.Select)
    name = forms.CharField(max_length=255, required=False, widget=forms.TextInput(attrs={'placeholder': 'Your Name'}))
    email = forms.EmailField(max_length=255, required=False, widget=forms.EmailInput(attrs={'placeholder': 'E-mail'}))
    country = forms.CharField(max_length=255,
                              widget=forms.TextInput(attrs={'id': 'country_select', 'placeholder': 'Your country'}))
    text = forms.CharField(required=False,
                           widget=forms.Textarea(attrs={'placeholder': 'Comment', 'cols': 30, 'row': 10}))
    phone = forms.CharField(required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'mobile_phone'}))


class SubmitForm(forms.Form):
    email = forms.EmailField(max_length=100,
                             widget=forms.EmailInput(attrs={'placeholder': 'Email'}))
    phone = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Phone'}))
    text = forms.CharField(max_length=1000,
                           widget=forms.Textarea(attrs={'cols': 30, 'rows': 10, 'placeholder': 'Your message'}))
