# coding=utf-8
from __future__ import unicode_literals

from django.db import models


# Create your models here.

class ThingsToDo(models.Model):
    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

    image = models.ImageField(upload_to='image/things_to_do', verbose_name='Изображение')
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(max_length=750, verbose_name='Краткое описание')

    def __unicode__(self):
        return self.title
