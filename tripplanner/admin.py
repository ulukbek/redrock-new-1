from django.contrib import admin
from tripplanner.models import *

# Register your models here.
admin.site.register(Hotels)
admin.site.register(Transport)
admin.site.register(Food)
admin.site.register(TripCategory)
